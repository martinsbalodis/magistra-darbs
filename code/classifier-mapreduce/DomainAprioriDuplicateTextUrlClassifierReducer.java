public class DomainAprioriDuplicateTextUrlClassifierReducer extends MapReduceBase implements Reducer<Text, Text, Text, NullWritable> {
    
    private static final NullWritable nullRes = NullWritable.get();
    private Text resultRule = new Text();
    
    @Override
    public void reduce(Text domain, Iterator<Text> values, OutputCollector<Text, NullWritable> output, Reporter reporter) throws IOException {
        String domainStr = domain.toString();
        Classifier classifier = new Classifier();
        
        while (values.hasNext()) {
            Text value = values.next();
            String valuesStr = value.toString();
            UrlRecord url = UrlRecord.makeFromClassifierRecord(valuesStr);
            classifier.addUrlRecord(url);
        }
        classifier.classify();
        List<String[]> rules = classifier.getClassifierResults();
        for(String[] rule : rules) {
            String ruleStr = domainStr+"\u0001";
            for(int i = 0;i<rule.length;i++) {
                ruleStr+=rule[i];
                if(i+1<rule.length) ruleStr+="\u0001";
            }
            resultRule.set(ruleStr);
            output.collect(resultRule, nullRes);
        }
    }
}
