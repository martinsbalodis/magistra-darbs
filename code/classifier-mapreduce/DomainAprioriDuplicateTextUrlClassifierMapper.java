public class DomainAprioriDuplicateTextUrlClassifierMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {
    
	private Text resultKey = new Text();
    private static final Pattern urlPattern = Pattern.compile("https?://([^/]+)(/[^?]*)(\\?(.*))?");
    
	@Override
    public void map(LongWritable key, Text input, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
        
        String line = input.toString();
        String[] inputParts = line.split("\u0001");
        if(inputParts.length != 2) {
            System.err.println("incorrect input line");
            return;
        }
        
        String url = inputParts[0];
        Matcher m = urlPattern.matcher(url);
        if(!m.matches()) {
            System.err.println("invalid url "+url);
            return;
        }

        String domain = m.group(1);
        resultKey.set(domain);
        output.collect(resultKey, input);
    }
}
