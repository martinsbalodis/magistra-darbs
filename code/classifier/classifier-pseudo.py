# Saraksts ar sākotnējām saišu daļu kortežiem
classifier_records = []
# Saišu filtri, kas tiek iegūti pēc darba
result_filters = []

# Saišu daļu ierobežojums kortežos
for(dz=1;dz<=5;dz++):
	
	# Nākošā līmeņa saišu kortežu pamat korteži
	next_classifier_record_sources = []

	# Esošo saišu daļu kortežu pārbaude
	for record in classifier_records:
		# Ieraksts iztur saišu filtrēšanas likuma slieksni
		if record.can_be_supported():
			# Saišu daļu kombinācija ir derīga kā filtrs
			if record.is_supported():
				result_filters.add(record)
			else:
				next_classifier_record_sources.add(record)

	# Izveido sarakstu ar saišu kortežiem nākošajam līmenim
	classifier_records = []
	for next_classifier_record_sources as record1:
		for next_classifier_record_sources as record2:
			# Apvieno divus saišu daļu kortežus jaunā 
			# kortežā ar garumu dz+1
			if(can_be_combined(record1, record2):
				classifier_records[] = combine(record1, record2)
