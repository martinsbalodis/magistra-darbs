package lv.timeklis.ml.aprioriurlclassifier;

import java.util.*;

public class ClassifierRecord implements Comparable<ClassifierRecord> {

    /**
     * Unique text url count
     */
    public int accepted = 0;

    /**
     * Duplicate text url count. We are searching for the rejected urls.
     */
    public int rejected = 0;
    public int[] urlParts;
    private String id;

    /**
     * List of urls that this classifier record was created from
     */
    public ArrayList<UrlRecord> urls;

    public static int minSupportedUrls = 50;
    public static float requiredRejectedUrlProportion = 0.95f;
    public static float minRejectedUrlProportion = 0.01f;
    public static int minRejectedUrls;

    static {
        updateMinRejectedUrls();
    }
    
    public static void updateMinRejectedUrls() {

        float minSuppoertedUrls = (float) ClassifierRecord.minSupportedUrls;
        minRejectedUrls = (int) Math.ceil(minSuppoertedUrls* requiredRejectedUrlProportion);
    }

    {
        urls = new ArrayList<UrlRecord>();
    }

    public ClassifierRecord() {

    }

    public ClassifierRecord(int accepted) {
        
        inc(accepted);
    }

    public ClassifierRecord(int accepted, int rejected) {

        inc(accepted, rejected);
    }

    public ClassifierRecord(int[] urlParts) {

        setUrlParts(urlParts);
    }

    public ClassifierRecord(int[] urlParts, int accepted) {

        setUrlParts(urlParts);
        inc(accepted);
    }

    public ClassifierRecord(int[] urlParts, int accepted, int rejected) {

        setUrlParts(urlParts);
        inc(accepted, rejected);
    }

    private void setUrlParts(int[] urlParts) {
        this.urlParts = urlParts;
    }
    
    public void addUrl(UrlRecord url) {
        urls.add(url);
        this.inc(url.accepted);
    }

    /**
     * Update record counters
     * @param accepted
     */
    public void inc(int accepted) {
        if(accepted == 1) {
            this.accepted += 1;
        }
        else {
            this.rejected += 1;
        }
    }

    public void inc(int accepted, int rejected) {
        this.accepted += accepted;
        this.rejected += rejected;
    }

    /**
     * Returns true if the rejected page count is enough
     *
     * @return boolean
     */
    public boolean canBeSupported() {
        boolean canBeSupported = accepted+rejected >= minSupportedUrls
                && rejected >= minRejectedUrls;
        
        if(canBeSupported) {
            float rejected = (float) this.rejected;
            float accepted = (float) this.accepted;
            
            boolean hasMinRejectedUrls = rejected / (accepted + rejected) > minRejectedUrlProportion;
            return hasMinRejectedUrls;
        }
        else {
            return false;
        }
    }

    /**
     * Returns true if the rejected page ratio is enough
     *
     * @return boolean
     */
    public boolean isSupported() {
        if(!canBeSupported()) {
            return false;
        }
        float rejected = (float) this.rejected;
        float accepted = (float) this.accepted;

        boolean isSupported = rejected / (accepted + rejected) > requiredRejectedUrlProportion;
        return isSupported;
    }

    public String getId() {
        if(id == null) {
            id = "";
            for(int urlPart: urlParts) {
                id+="|"+Integer.toString(urlPart);
            }
        }
        return id;
    }

    public String toString() {
        return getId() + "|" + Integer.toString(accepted) + "|" + Integer.toString(rejected);
    }

    /**
     * Compares Records by their url parts
     * @param o
     * @return
     */
    @Override
    public int compareTo(ClassifierRecord o) {

        for(int i = 0;i< urlParts.length;i++) {
            if(urlParts[i] != o.urlParts[i]) {
                return urlParts[i]-o.urlParts[i];
            }
        }
        return 0;
    }

    public void inc(ClassifierRecord classifierRecord) {
        this.accepted +=classifierRecord.accepted;
        this.rejected +=classifierRecord.rejected;
    }
}
