package lv.timeklis.ml.aprioriurlclassifier;

import java.util.*;

/**
 * List of unique classifier records
 */
public class ClassifierResultTree {

    HashMap<String, HashMap> urlPartTree;

    {
        urlPartTree = new HashMap<String, HashMap>();
    }

    public void add(String[] urlParts) {

        // sort just in case.
        Arrays.sort(urlParts);

        if(has(urlParts)) {
            return;
        }

        HashMap childUrlPartTree;
        HashMap parentUrlPartTree = urlPartTree;

        for(String urlPart : urlParts) {
            childUrlPartTree = (HashMap) parentUrlPartTree.get(urlPart);
            if(childUrlPartTree == null) {
                childUrlPartTree = new HashMap<String, HashMap>();
                parentUrlPartTree.put(urlPart, childUrlPartTree);
            }
            parentUrlPartTree = childUrlPartTree;
        }
    }

    public boolean has(String[] urlParts) {

        Arrays.sort(urlParts);

        return has(urlParts, 0, this.urlPartTree);
    }
    
    public boolean has(String[] urlParts, int level, HashMap<String, HashMap> parentUrlPartTree) {
        
        String urlPart = urlParts[level];

        HashMap childUrlPartTree;
        int nextLevel = level+1;
        
        // check direct match
        childUrlPartTree = parentUrlPartTree.get(urlPart);
        if(childUrlPartTree != null) {
            if(nextLevel == urlParts.length) {
                return true;
            }

            boolean has = has(urlParts, level+1, childUrlPartTree);
            if(has) {
                return true;
            }
        }
        
        // check similar directory matches
        if(urlPart.startsWith("/")) {
            String[] directoryParts = urlPart.split("/");
            String currentDirectoryPath = "";
            for(int i = 1; i < directoryParts.length; i++) {
                currentDirectoryPath+="/"+directoryParts[i];

                childUrlPartTree = parentUrlPartTree.get(currentDirectoryPath);
                if(childUrlPartTree != null) {
                    if(nextLevel == urlParts.length) {
                        return true;
                    }

                    boolean has = has(urlParts, level+1, childUrlPartTree);
                    if(has) {
                        return true;
                    }
                }
            }
        }
        
        // check similar query matches
        // if we are searching for ?a=1, then we also might search for ?a=
        else if(urlPart.startsWith("?")) {
            
            int pos = urlPart.indexOf('=');
            if(pos > 0 && urlPart.length() > pos+1) {

                childUrlPartTree = parentUrlPartTree.get(urlPart.substring(0, pos+1));
                if(childUrlPartTree != null) {
                    if(nextLevel == urlParts.length) {
                        return true;
                    }

                    boolean has = has(urlParts, level+1, childUrlPartTree);
                    if(has) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
