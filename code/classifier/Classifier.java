package lv.timeklis.ml.aprioriurlclassifier;

import java.util.*;

public class Classifier {

    /**
     * Maximum amount of url parts that can be in a single rule
     */
    public static final int maxUrlParts = 5;

    /**
     * List of all unique url parts that could be used by classifier.
     */
    public ArrayList<String> urlPartIndex;

    public int lastUrlId = 0;

    public ClassifierResultTree resultTree;
    
    public List<String[]> resultList;
    
    /**
     * List of urls added used by the classifier
     */
    ArrayList<UrlRecord> urlRecords;
    
    {
        urlPartIndex = new ArrayList<String>();
        urlRecords = new ArrayList<UrlRecord>();
        resultTree = new ClassifierResultTree();
        resultList = new ArrayList<String[]>();
    }

    public int getUrlPartId(String urlPart) {
        int id = Collections.binarySearch(urlPartIndex, urlPart);
        return id;
    }
    
    public void addUrlRecord(UrlRecord urlRecord) {

        urlRecord.setId(lastUrlId++);
        urlRecords.add(urlRecord);
    }

    /**
     * creates a list of unique url parts
     */
    public void generateUrlPartIndex() {

        // Helps to identify urlPart uniqueness
        TreeSet<String> uniqueUrlParts = new TreeSet<String>();

        // find all unique UrlParts
        for(UrlRecord urlRecord : urlRecords) {
            for(String urlPart: urlRecord.urlParts) {

                if(!uniqueUrlParts.contains(urlPart)) {
                    uniqueUrlParts.add(urlPart);
                }
            }
        }

        // create unique urlPart index
        for(String urlPart : uniqueUrlParts) {
            urlPartIndex.add(urlPart);
        }
        // should be already sorted
        Collections.sort(urlPartIndex);
    }

    /**
     * Generate classifier record list from urls and urlparts
     * @return
     */
    public List<ClassifierRecord> getInitialClassifierRecords() {

        HashMap<Integer, ClassifierRecord> records = new HashMap<Integer, ClassifierRecord>();

        // initialize array or Classifier Records from UrlPartIndex
        for(int urlPartId = 0;urlPartId < urlPartIndex.size();urlPartId++) {
            records.put(urlPartId, new ClassifierRecord(new int[]{urlPartId}));
        }

        // count all url occurences of url parts and store them in url records

        for(UrlRecord urlRecord : urlRecords) {
            for(String urlPart: urlRecord.urlParts) {

                int urlPartId = getUrlPartId(urlPart);
                ClassifierRecord record = records.get(urlPartId);
                record.addUrl(urlRecord);
            }
        }

        List<ClassifierRecord> resultRecords = new ArrayList<ClassifierRecord>(records.values());
        // hashmap wasn't in sorted order so we need to sort the list
        Collections.sort(resultRecords);

        return resultRecords;
    }

    /**
     * Start url classification
     */
    public void classify() {

        generateUrlPartIndex();
        List<ClassifierRecord> initialClassifierRecords = getInitialClassifierRecords();

        classify(initialClassifierRecords, 1);
    }

    /**
     * Classify records level by level
     * @param records
     * @param level
     */
    private void classify(List<ClassifierRecord> records, int level) {

        // list of records that need further classification
        List<ClassifierRecord> nextLevelRecordSourceRecords = new ArrayList<ClassifierRecord>();

        // remove unsupported records and supported records
        for(ClassifierRecord record : records) {
            if(record.canBeSupported()) {
                if(record.isSupported()) {
                    addClassified(record);
                }
                else {
                    nextLevelRecordSourceRecords.add(record);
                }
            }
        }
        
        // limit maximum url parts in classifiers
        if(level+1 > maxUrlParts) {
            return;
        }

        List<ClassifierRecord> nextLevelRecords = iterateClassificationCombinations2(nextLevelRecordSourceRecords, level + 1);
        if(nextLevelRecords.size() > 0) {
            classify(nextLevelRecords, level+1);
        }
    }
    
    public String[] getUrlPartsFromUrlPartIdList(int[] urlPartIds) {
        
        String[] urlParts = new String[urlPartIds.length];
        for(int i=0;i<urlPartIds.length;i++) {
            urlParts[i] = urlPartIndex.get(urlPartIds[i]);
        }
        
        return urlParts;
    }

    /**
     * Check that record isn't already classified.
     * @param record
     * @return
     */
    public boolean hasClassifiedRecordList(ClassifierRecord record) {

        String[] urlParts = getUrlPartsFromUrlPartIdList(record.urlParts);
        return resultTree.has(urlParts);
    }

    public void addClassified(ClassifierRecord record) {
        
        String[] urlParts = getUrlPartsFromUrlPartIdList(record.urlParts);
        
        if(!resultTree.has(urlParts)) {
            resultTree.add(urlParts);
            resultList.add(urlParts);
        }
    }

    private List<ClassifierRecord> iterateClassificationCombinations2(List<ClassifierRecord> records, int maxUrlPartCount) {
        
        int recordCount = records.size();
        ClassifierRecord record1;
        ClassifierRecord record2;
        int[] currentUrlPartCombination = new int[maxUrlPartCount];
        int[] searchUrlPartCombination = new int[maxUrlPartCount-1];
        ClassifierRecord searchClassifierRecord = new ClassifierRecord(searchUrlPartCombination);
        List<ClassifierRecord> resultClassifierRecords = new ArrayList<ClassifierRecord>();
        
        for(int i = 0;i < recordCount;i++) {
            record1 = records.get(i);
            for(int j = i+1;j<recordCount;j++) {
                record2 = records.get(j);

                // skip uncombinable classifier records
                if(!canBeCombined(record1, record2)) {
                    break;
                }
                
                // combine two record url parts in a resulting record
                for(int v = 0;v<record1.urlParts.length;v++) {
                    currentUrlPartCombination[v] = record1.urlParts[v];
                }
                currentUrlPartCombination[record1.urlParts.length] = record2.urlParts[record1.urlParts.length-1];

                // check whether the combination doesn't have illogical url part
                // pairs. Like (?a=, ?a=1) or (?a=1, ?a=2)
                String prevUrlPart = urlPartIndex.get(currentUrlPartCombination[0]);
                boolean skipUrlCombination = false;
                for(int p = 1;p<maxUrlPartCount;p++) {
                    String currentUrlPart = urlPartIndex.get(currentUrlPartCombination[p]);
                    if(currentUrlPart.startsWith("?") && prevUrlPart.startsWith("?")) {
                        int pos = prevUrlPart.indexOf('=');
                        if(pos > 0) {
                            if(currentUrlPart.startsWith(prevUrlPart.substring(0, pos+1))) {
                                skipUrlCombination = true;
                                break;
                            }
                        }
                    }
                    else if(currentUrlPart.startsWith("/") && prevUrlPart.startsWith("/")) {
                        skipUrlCombination = true;
                        break;
                    }
                    prevUrlPart = currentUrlPart;
                }
                if(skipUrlCombination) {
                    continue;
                }

                // check whether all other combinations exist
                boolean allCombinationsExist = true;
                for(int c = maxUrlPartCount-3; c >= 0; c--) {
                    for(int k = 0,l=0;k<maxUrlPartCount;k++) {
                        if(c != k) {
                            searchUrlPartCombination[l] = currentUrlPartCombination[k];
                            l++;
                        }
                    }
                    int index = Collections.binarySearch(records, searchClassifierRecord);
                    if(index < 0) {
                        allCombinationsExist = false;
                        break;
                    }
                }
                if(!allCombinationsExist) {
                    continue;
                }
                
                // create a list of common urls
                ClassifierRecord resultRecord = new ClassifierRecord(currentUrlPartCombination.clone());
                
                // combine urls from all ClassifierRecords to calculate new accepted and rejected stats
                addIntersectionUrls(record1, record2, resultRecord);
                
                if(!resultRecord.canBeSupported()) {
                    continue;
                }
                
                // add new record
                resultClassifierRecords.add(resultRecord);
            }
        }
        return resultClassifierRecords;
    }
    
    public void addIntersectionUrls(ClassifierRecord record1, ClassifierRecord record2, ClassifierRecord resultRecord) {

        ArrayList<UrlRecord> urls1 = record1.urls;
        ArrayList<UrlRecord> urls2 = record2.urls;
        int size1 = urls1.size();
        int size2 = urls2.size();
        
        int i=0,j=0;
        UrlRecord url1 = urls1.get(i);
        UrlRecord url2 = urls2.get(j);

        for(;;) {

            int cmp = url1.compareTo(url2);
            if(cmp == 0) {
                resultRecord.addUrl(url1);
                i++;
                j++;
                if(i>=size1 || j>=size2) {
                    return;
                }
                
                url1 = urls1.get(i);
                url2 = urls2.get(j);
            }
            else if(cmp < 0) {
                i++;
                if(i>=size1) {
                    return;
                }
                url1 = urls1.get(i);
            }
            else {
                j++;
                if(j>=size2) {
                    return;
                }
                url2 = urls2.get(j);
            }
        }
    }

    /**
     * Check whether these two classifier records have common n-1 url parts
     * @return
     */
    public boolean canBeCombined(ClassifierRecord record1, ClassifierRecord record2) {

        // record1 must be < record2
        // this helps to skip multiple similar combinsations
        int[] urlparts1 = record1.urlParts;
        int[] urlparts2 = record2.urlParts;

        for(int i = 0;i<urlparts1.length-1;i++) {
            if(urlparts1[i] != urlparts2[i]) {
                return false;
            }
        }

        return urlparts1[urlparts1.length-1] < urlparts2[urlparts1.length-1];
    }
    
    public List<String[]> getClassifierResults() {
        return resultList;
    }
}
