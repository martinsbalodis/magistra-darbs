package lv.timeklis.ml.aprioriurlclassifier;

import java.util.TreeSet;
import java.util.regex.Matcher;

public class UrlRecord implements Comparable<UrlRecord> {

    public TreeSet<String> urlParts;
    public int accepted;
    public int id;
    public String domain;
    
    public static boolean enableDirecotories = true;
    public static boolean enableDirectoryParts = true;
    public static boolean enableQueryParaeters = true;
    public static boolean enableUniversalQueryParaeters = true;

    private static final java.util.regex.Pattern urlPattern = java.util.regex.Pattern.compile("https?://([^/]+)(/[^?]*)(\\?(.*))?");

    public UrlRecord() {
        urlParts = new TreeSet<String>();
    }
    
    public UrlRecord(TreeSet<String> urlParts, int accepted) {
        this.urlParts = urlParts;
        this.accepted = accepted;
    }

    public UrlRecord(String[] urlParts, int accepted) {

        this.urlParts = new TreeSet<String>();

        for(String urlPart : urlParts) {
            this.urlParts.add(urlPart);
        }

        this.accepted = accepted;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean hasUrlPart(String urlPart) {
        return urlParts.contains(urlPart);
    }

    public static UrlRecord makeFromClassifierRecord(String dataStr) {

        UrlRecord urlRecord = new UrlRecord();
        String[] dataStrParts = dataStr.split("\u0001");

        urlRecord.accepted = Integer.parseInt(dataStrParts[1]);

        String url = dataStrParts[0];
        urlRecord.parseUrl(url);

        return urlRecord;
    }

    public static UrlRecord makeFromUrl(String url) {
        UrlRecord urlRecord = new UrlRecord();
        urlRecord.parseUrl(url);
        return urlRecord;
    }

    public static UrlRecord makeFromUrl(String url, int accepted) {
        UrlRecord urlRecord = makeFromUrl(url);
        urlRecord.accepted = accepted;
        return urlRecord;
    }

    private void parseUrl(String url) {
        Matcher m = urlPattern.matcher(url);
        if(!m.matches()) {
            System.err.println("invalid url "+url);
        }

        String domain = m.group(1);
        this.domain = domain;

        if(enableDirecotories) {
            String pathFull = m.group(2);
            if(enableDirectoryParts) {
                String[] pathParts = pathFull.split("/");
                String path = "/";
                for (int i = 1; i < pathParts.length; i++) {
                    String pathPart = pathParts[i];
                    path = path + (i <= 1 ? "" : "/") + pathPart;
                    urlParts.add(path);
                }
            }
            else if(!pathFull.equals("/")) {
                urlParts.add(pathFull);
            }
        }

        if(enableQueryParaeters) {
            String query = m.group(4);
            String[] queryParts;
            if(query != null) {
                queryParts = query.split("&");
            }
            else {
                queryParts = new String[] {};
            }
            
            for (String queryPart : queryParts) {
                if (queryPart.length() == 0) {
                    continue;
                }
                // add ?a=
                if(enableUniversalQueryParaeters) {
                    int pos = queryPart.indexOf('=');
                    if (pos > 0) {
                        urlParts.add("?" + queryPart.substring(0, pos + 1));
                    }
                }

                // add ?a=1
                urlParts.add("?" + queryPart);
            }
        }
    }

    @Override
    public int compareTo(UrlRecord o) {
        return id-o.id;
    }
}
