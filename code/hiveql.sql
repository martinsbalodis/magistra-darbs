SELECT million, 
	COUNT(*) AS found_total, 
	SUM(unique) AS count_unique, 
	SUM(url_filter("hdfs:///user/martinsbalodis/magistra-darbs/filter-rules.list", page_outlinks)) AS count_filtered
FROM plasuma_2015_04_26_urls_first_found_only_unique_millions
WHERE hops_to_page < 10
GROUP BY million
ORDER BY million
