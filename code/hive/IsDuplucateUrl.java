public final class UrlMatchFilter extends UDF {
    private static HashMap<String, PatternTree> patternTrees = new HashMap<String, PatternTree>();
    public IntWritable evaluate(final Text patternFileText, final Text urlText) {

        String patternTreeFile = patternFileText.toString();
        PatternTree patternTree = patternTrees.get(patternTreeFile);
        
        if(patternTree == null) {
            patternTree = new PatternTree();
            try {
                String patternFileStr = patternFileText.toString();
                Path pt = new Path(patternFileStr);
                FileSystem fs = FileSystem.get(new Configuration());
                BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(pt)));
                patternTree.buildTree(br);
                patternTrees.put(patternTreeFile, patternTree);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        String url = urlText.toString();
		return new IntWritable(patternTree.matchUrl(url)?1:0);
    }
}
