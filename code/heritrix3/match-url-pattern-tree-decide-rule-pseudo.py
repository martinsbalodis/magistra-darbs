# class MatchUrlPatternTreeDecideRule:

# datu struktūra kurā tiek ielādēts saišu filtrs
domain_tree

# Heritrix inicializē saišu filtru
setTextSouce(source):
	# Atmiņā tiek izveidota saišu filtra datu struktūra
	domain_tree = parse_source(source)

# Heritrix izsauc saites filtrēšanas pārbaudi
evaluate(uri):
	
	# vispirms tiek pārbaudīts vai saites domēns ir filtrā
	domain = uri.domain
	parent_part_tree = domain_tree.get(domain)
    if parent_part_tree == null:
        return false

	# tiek apstaigāts domēna saišu daļu koks, izmantojot 
	# saites daļas
	url_parts = uri.url_parts
	for url_part in url_parts:
        child_part_tree = parent_part_tree.get(url_part)
        if child_part_tree != null:
            parent_part_tree = child_part_tree
	
	# Ja izejot cauri visām saites daļām tika atrasta filtra
	# koka lapa, tad saite ir filtrējama ar sašu daļām, kas 
	# tika apstaigātas kokā.
    return parent_part_tree.is_last()

