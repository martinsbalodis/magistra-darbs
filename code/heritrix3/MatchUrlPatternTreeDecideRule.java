package org.archive.modules.deciderules;

import org.archive.io.ReadSource;
import org.archive.modules.CrawlURI;
import org.springframework.beans.factory.annotation.Required;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;

/**
 * Rule applies configured decision to any URIs which do 
 * match the supplied patterns given in a configuration file
 */
public class MatchUrlPatternTreeDecideRule extends PredicatedDecideRule {

    PatternTree tree;
    
    private static final Logger LOGGER =
            Logger.getLogger(AddRedirectFromRootServerToScope.class.getName());

    /**
     * Text from which to extract url patterns
     */
    protected ReadSource textSource = null;

    {
        tree = new PatternTree();
    }
    
    public ReadSource getTextSource() {
        return textSource;
    }
    
    @Required
    public void setTextSource(ReadSource seedsSource) {
        this.textSource = seedsSource;
        BufferedReader reader = new BufferedReader(textSource.obtainReader());

        try {
            tree.buildTree(reader);
        } catch (IOException e) {
            String msg = "IOException while parsing url pattern file. " 
                    + e.getMessage();
            LOGGER.warning(msg);
        }
    }

    @Override
    protected boolean evaluate(CrawlURI uri) {
        boolean match = tree.matchUrl(uri.toString());
        return match;
    }

    public static class PatternTree {

        private HashMap<String, PathTree> domainTree;

        public PatternTree() {

            domainTree = new HashMap<String, PathTree>();
        }

        public void buildTree(File treePatternDataFile) throws IOException {

            // Open the file
            FileInputStream fstream = new FileInputStream(treePatternDataFile);
            InputStreamReader iSR = new InputStreamReader(fstream);
            BufferedReader br = new BufferedReader(iSR);

            buildTree(br);
        }

        public void buildTree(BufferedReader br) throws IOException {

            String strLine;

            //Read File Line By Line
            while ((strLine = br.readLine()) != null)   {
                // Print the content on the console
                this.addPatternToTree(strLine);
            }

            //Close the input stream
            br.close();
        }

        public void addPatternToTree(String patternStr) {

            Pattern pattern = Pattern.createFromParsedUrlPattern(patternStr);
            add(pattern);
        }

        public void add(Pattern pattern) {

            if(has(pattern)) {
                return;
            }

            PathTree parentPathTree = domainTree.get(pattern.domain);
            if(parentPathTree == null) {
                parentPathTree = new PathTree<String, PathTree>();
                domainTree.put(pattern.domain, parentPathTree);
            }

            PathTree childPathTree;

            for(String urlPath : pattern.urlParts) {
                childPathTree = (PathTree) parentPathTree.get(urlPath);
                if(childPathTree == null) {
                    childPathTree = new PathTree<String, PathTree>();
                    parentPathTree.put(urlPath, childPathTree);
                }
                parentPathTree = childPathTree;
            }
        }

        public boolean has(Pattern pattern) {

            PathTree parentPathTree = domainTree.get(pattern.domain);
            if(parentPathTree == null) {
                return false;
            }

            PathTree childPathTree;

            for(String urlPath : pattern.urlParts) {
                childPathTree = (PathTree) parentPathTree.get(urlPath);
                if(childPathTree != null) {
                    parentPathTree = childPathTree;
                }
            }
            return parentPathTree.isLast();
        }

        public boolean matchUrl(String url) {

            Pattern pattern = Pattern.createFromUrl(url);
            
            // allow dns or other non http requests
            if(pattern == null) {
                return false;
            }
            return has(pattern);
        }
    }

    public static class Pattern {

        public String domain;
        public String[] urlParts;

        private static final java.util.regex.Pattern urlPattern = 
                java.util.regex.Pattern.compile("https?://([^/]+)(/[^?]*)(\\?(.*))?");

        public static Pattern createFromParsedUrlPattern(String patternStr) {

            Pattern pattern = new Pattern();

            // example pattern:
            // www.verbasco.lv\u0001/lv/produkti\u0001sort_by=sell_price
            // \u0001sort_order=ASC
            String[] patternStrParts = patternStr.split("\u0001");
            pattern.domain = patternStrParts[0];
            pattern.urlParts = new String[patternStrParts.length-1];

            for(int i = 1;i<patternStrParts.length;i++) {
                pattern.urlParts[i-1] = patternStrParts[i];
            }

            Arrays.sort(pattern.urlParts);

            return pattern;
        }

        public static Pattern createFromUrl(String url) {

            Pattern pattern = new Pattern();

            Matcher m = urlPattern.matcher(url);
            if(!m.matches()) {
                System.err.println("invalid url "+url);
                return null;
            }

            String domain = m.group(1);
            pattern.domain = domain;
            String pathFull = m.group(2);
            String query = m.group(4);
            String[] pathParts = pathFull.split("/");
            String[] queryParts;
            if(query != null) {
                queryParts = query.split("&");
            }
            else {
                queryParts = new String[] {};
            }

            pattern.urlParts = new String[pathParts.length+queryParts.length];

            int k = 0;
            String path = "";
            for(int i = 0;i<pathParts.length;i++) {
                String pathPart = pathParts[i];
                path = path+(i==0?"":"/")+pathPart;
                pattern.urlParts[k]=path;
                k++;
            }

            for (String queryPart : queryParts) {
                pattern.urlParts[k] = queryPart;
                k++;
            }

            Arrays.sort(pattern.urlParts);

            return pattern;
        }
    }

    public static class PathTree<String, PathTree> 
            extends HashMap<String, PathTree> {

        public boolean isLast() {

            return this.size() == 0;
        }
    }
}
